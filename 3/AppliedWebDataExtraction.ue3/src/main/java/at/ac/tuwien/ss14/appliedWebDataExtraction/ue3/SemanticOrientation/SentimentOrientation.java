package at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SentimentOrientation {
	// Number of hits for "poor"
	private static double hitsPoor = -1;
	private static final String POOR = "poor";
	
	// number of hits for "excellent"
	private static double hitsExcellent = -1;
	private static final String EXCELLENT = "excellent";
	
	// Set of strings to evaluate for semantic orientation
	private Collection<String> phraselist = new HashSet<String>();
	
	// Threadpool for phrase SO lookup parallelisation
	private ExecutorService executors = Executors.newFixedThreadPool(4);
	
	double sentimentAvg = 0;
	
	/**
	 * Add a phrase
	 * @param str
	 */
	public void addString(String str){
		phraselist.add(str);
	}
	
	/**
	 * Add a list of phrases
	 * @param str
	 */
	public void addString(List<String> str){
		phraselist.addAll(str);
	}
	
	/**
	 * clean the list of phrases
	 */
	public void clearStrings(){
		phraselist.clear();
	}
	
	/**
	 * Returns true if the average Sentiment Orientation 
	 * of the phrases is positive, otherwise false
	 * This process will take a while since the SO of each phrase is queried
	 * @return
	 * @throws IOException 
	 */
	public boolean isPositiveReview(){
		return sentimentAvg > 0;		
	}
	
	public void calculateSentimentOrientation() throws IOException{
		System.out.println("Perform Google Searches");
		init_Poor_Excellent();
		
		double sentimentSum = 0;
		
		/*
		 *  SO(phrase) = log_2( 
		 *		 			hits(Phrase NEAR Excellent) * hits(poor) /
		 *		 	  		hits(Phrase NEAR Poor) * hits(excellent) )
		 *
		 * skip phrase when both hits(Phrase NEAR Excellent) < 4 and hits(Phrase NEAR Poor) < 4
		 */
		
		
		// Add lookup jobs to executor for parallel execution
		for (String phrase : phraselist) {
			executors.execute(new GoogleLookupRunnable(phrase, POOR));
			executors.execute(new GoogleLookupRunnable(phrase, EXCELLENT));
		}
		
		System.out.println("Wait for results");
		
		// Wait for completion of all jobs with an "infinite" timeout
		executors.shutdown();
		try {
			executors.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("Calculate SO");
		
		double sentiment, nearPoor, nearExcellent;
		double phrasesCount = 0;
		// Result counts now being queried from the Cache
		for (String phrase : phraselist) {
			nearPoor = GoogleSearch.getSearchResults(phrase, POOR);
			nearExcellent = GoogleSearch.getSearchResults(phrase, EXCELLENT);
			
			if (nearPoor < 4 && nearExcellent < 4)
				continue;
			
			sentiment = Math.log(
					(nearExcellent * hitsPoor ) / 
					(nearPoor * hitsExcellent )
						) / Math.log(2);
			
			sentimentSum += sentiment;
			phrasesCount++;
		}
		
		sentimentAvg = sentimentSum / phrasesCount;
	}
	
	private void init_Poor_Excellent() throws IOException{
		if (hitsExcellent == -1)
			hitsExcellent = GoogleSearch.getSearchResults(EXCELLENT);
		if (hitsPoor == -1)
			hitsPoor = GoogleSearch.getSearchResults(POOR);
	}
	
	public double getAverageSentimentOrientation(){
		return sentimentAvg;
	}
	
}
