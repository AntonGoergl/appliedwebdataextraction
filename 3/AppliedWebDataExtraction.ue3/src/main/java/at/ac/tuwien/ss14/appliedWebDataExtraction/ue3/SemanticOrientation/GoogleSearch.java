package at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * 
 * @author Anton
 * Performs a Search on Google and returns the number of results
 * The result counts of each search will be cached in memory to 
 * prevent sending the same request multiple times and to speed the analysis up
 */
public class GoogleSearch {	
	
	private static final int MAX_RETRIES = 5;
	// Cache how many results each request got
	private static Map<String, Long> cache = new ConcurrentHashMap<String, Long>();
	
	/**
	 * Returns the number of Search results for a given String
	 * Performs the following search: â€œ$wordâ€�
	 * @param str Searchstring
	 * @return
	 * @throws IOException 
	 */
	public static double getSearchResults(String str) throws IOException{
		return getSearchResults_Search(String.format("\"%s\"", str.toLowerCase()));
	}
	
	/**
	 * Returns the number of Search results of a given String near another given String
	 * Performs the following search: â€œ$wordâ€� AROUND(3) "$near",
	 * @param str String that should occur near $near
	 * @param near String $str should be close to
	 * @return
	 * @throws IOException 
	 */
	public static double getSearchResults(String str, String near) throws IOException{
		return getSearchResults_Search(String.format("\"%s\" AROUND(3) \"%s\"", str.toLowerCase(), near.toLowerCase()));		
	}
	
	private static double getSearchResults_Search(String search) throws IOException{
		boolean error = false;
		int tries = 0;
		
		//while (error && tries < MAX_RETRIES)
		//try {
			return getSearchResults_performSearch(search);
		/*} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error = true;
			tries++;
			
			if (tries == MAX_RETRIES -1)
				throw e;
		}*/
		
		//return 0.01;
	}
	
	private static double getSearchResults_performSearch(String search) throws IOException{
		// Cache lookup
		Long resultCount = cache.get(search);
		if (resultCount != null)
			return resultCount;
		
		WebDriver driver = new FirefoxDriver();		
		driver.get("http://www.google.com/#q=" + search);
		WebElement resultsDiv = null;
		
		// Wait until element is found, or 30 sec timeout
		long end = System.currentTimeMillis() + 30000;
		//while (System.currentTimeMillis() < end) {
		while(true){ // wait until element visible
			try {
            resultsDiv = driver.findElement(By.xpath("//div[@id='resultStats']"));
			} catch (NoSuchElementException e){ 
				continue;
			}
			
			
            if (resultsDiv.isDisplayed()) {
              break;
            }
        }
		driver.close();
		
		String[] result = resultsDiv.getText().split(" ");
		try {
			resultCount = Long.parseLong(result[1].replace(".", ""));
		} catch (NumberFormatException nfe) {
			resultCount = Long.parseLong(result[0].replace(".", ""));
		}
		
		// write in Cache
		cache.put(search, resultCount);
		
		System.out.println(String.format("Results: %d Search: %s", resultCount, search));
		
		// write in Cache
		cache.put(search, resultCount);
		
		System.out.println(String.format("Results: %d Search: %s", resultCount, search));
		
		// value correction + 0.01 as in assignment to prevent divisions by zero
		return 	resultCount + 0.01; 
	}
}
