package at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation;

import java.io.IOException;

public class GoogleLookupRunnable implements Runnable {
	private String phrase;
	private String near;
	private boolean success = false;
	
	public GoogleLookupRunnable(String phrase, String near) {
		this.phrase = phrase;
		this.near = near;
	}

	public void run() {
		try {
			GoogleSearch.getSearchResults(phrase, near);
			success = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			success = false;
		}
	}
	
	public boolean isSucceeded(){
		return success;
	}

}
