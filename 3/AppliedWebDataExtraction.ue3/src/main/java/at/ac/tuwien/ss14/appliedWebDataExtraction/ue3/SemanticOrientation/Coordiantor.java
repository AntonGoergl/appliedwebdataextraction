package at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;


public class Coordiantor {

	final static String SEPARATOR=";";
	public Map<Integer,Review> map=new HashMap<Integer, Coordiantor.Review>();
	
	public Coordiantor() {
	}
	
	
	public void execute(String pathFileCSVReviews) throws IOException{
		
		readFileCSV(pathFileCSVReviews);
		
		Set<Entry<Integer, Review>> entrySet = map.entrySet();
		for (Entry<Integer, Review> entry : entrySet) {
			Review review = entry.getValue();
			Tagger tagger=new Tagger(review.getReview());
			SentimentOrientation so=new SentimentOrientation();
			try {
				so.addString(tagger.getPhrases());
			} catch (Exception e) {
				e.printStackTrace();
			}
			so.calculateSentimentOrientation();
			review.setScoreSO(so.getAverageSentimentOrientation());
			review.setPositiveReview(so.isPositiveReview());
			System.out.println("Complete "+(entry.getKey()+1) +" of "+ entrySet.size());
		}
		buildResult();
	}


	/**
	 * Build a new file csv in which per each review show the result
	 * @throws IOException 
	 */
	private void buildResult() throws IOException {
		File file;
		int count=0;
		do{
			file=new File("src"+File.separator+"resource"+File.separator+"ris_"+(count++)+"_.csv");
		}while(file.exists());
		BufferedWriter bw=new BufferedWriter(new FileWriter(file));
		StringBuilder sb=new StringBuilder();
		sb.append("Review"); sb.append(SEPARATOR);
		sb.append("Score Reviewer"); sb.append(SEPARATOR);
		sb.append("Score SO"); sb.append("\n");
		
		Set<Entry<Integer, Review>> entrySet = map.entrySet();
		for (Entry<Integer, Review> entry : entrySet) {
			Review review = entry.getValue();
			sb.append(review.getReview()); sb.append(SEPARATOR);
			sb.append(review.getScoreReviewer()); sb.append(SEPARATOR);
			sb.append(review.isPositiveReview()); sb.append("\n");
		}
		bw.write(sb.toString());
		bw.close();
	}


	public void readFileCSV(String pathFileCSVReviews)
			throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(pathFileCSVReviews));
		String line;
		int count=0;
		while ((line = br.readLine()) != null) {
			try{
				String []split=line.split(SEPARATOR);
				
				if(split.length>1){
					map.put(count++, new Review(split[0],Double.valueOf(split[1])));
				}else{
					System.err.println("No valid review");
				}
			}catch(Exception e){}
		}
		br.close();
		System.out.println("Loaded "+map.size()+" reviews");
	}

	public class Review{
		String review;
		Double scoreReviewer;
		Double scoreSO;
		boolean positiveReview;
		public Review(String review, Double scoreReviewer) {
			super();
			this.review = review;
			this.scoreReviewer = scoreReviewer;
		}
		public String getReview() {
			return review;
		}
		public void setReview(String review) {
			this.review = review;
		}
		public Double getScoreReviewer() {
			return scoreReviewer;
		}
		public void setScoreReviewer(Double scoreReviewer) {
			this.scoreReviewer = scoreReviewer;
		}
		public Double getScoreSO() {
			return scoreSO;
		}
		public void setScoreSO(Double scoreSO) {
			this.scoreSO = scoreSO;
		}
		public boolean isPositiveReview() {
			return positiveReview;
		}
		public void setPositiveReview(boolean positiveReview) {
			this.positiveReview = positiveReview;
		}
		
	}
	
	public static void main(String[] args) {
		_initialize();
		
		Coordiantor c=new Coordiantor();
		try {
			c.execute("src"+File.separator+"resource"+File.separator+"toys.csv");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void _initialize() {
		try {
			// set up new properties object
	        FileInputStream propFile = new FileInputStream("systemProperties.txt");
	        Properties p = new Properties(System.getProperties());
	        p.load(propFile);
	        
	        // set the system properties
	        System.setProperties(p);
		 
	        System.out.println("gate.home: " + System.getProperty("gate.home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
