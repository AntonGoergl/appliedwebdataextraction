package at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.Gate;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class Tagger {

	String review;

	public Tagger(String review) {
		this.review = review;
	}

	public Tagger() {}
	
	/**
	 * Principal function in which find phrases by tagging
	 * 
	 * @return List of phrases 
	 * @throws GateException 
	 * @throws Exception 
	 */
	public List<String> getPhrases() throws GateException, Exception{
		ArrayList<String> result=new ArrayList<String>();
		 File gappFile = new File("src/resource/pos_tagger.gapp");
  		 
	     // The character encoding to use when loading the docments.  If null, the platform default encoding is used.
	     String encoding = null;     
	     if(gappFile.exists()){
	    	 
	     // initialise GATE - this must be done before calling any GATE APIs
	     try{
	     Gate.init();
	     }catch(Exception e){
	    	 
	     }
	     }
	     // load the saved application
	     CorpusController application = (CorpusController)PersistenceManager.loadObjectFromFile(gappFile);
	 
	     Corpus corpus = Factory.newCorpus("corpus_1");
	     
	     application.setCorpus(corpus);
	     
	     File f=new File("tmp");
	     FileWriter fw=new FileWriter(f);
	     fw.write(review+".");
	     fw.close();
//	     File docFile = new File("/Users/swobi/Desktop/_exercises_/review.txt");
//	     System.out.println("Processing document " + docFile + "...");
//	     Document doc = Factory.newDocument(f.toURI().toURL(), encoding);
//	     File docFile = new File("tmp");
	     System.out.println("Processing document " + f + "...");
	     Document doc = Factory.newDocument(f.toURI().toURL(), encoding);
//	     f.delete();

	     // put the document in the corpus
	     corpus.add(doc);
	     
	     // run the application (must be "corpus pipeline" or "conditional corpus pipeline")
	     application.execute();

	     // remove the document from the corpus again
	     corpus.clear();
	     f.delete();
	       
	     // we use tow Annotation Sets "Token" which contains the tokenized file and
	     // default (unnamed) AnnotationSet which contains the POS Tags
	     AnnotationSet TokenAnnots = doc.getAnnotations("Token");
	     AnnotationSet defaultAnnots = doc.getAnnotations();     
	 
	     // helper strings to store three consecutive tokens and their categories (=POS Tag)
	     // this means I can do all the "magic" in one loop
	     String first_token = "";
	     String second_token = "";
	     String third_token = "";
	     
	     String first_token_category = "";
	     String second_token_category = "";
	     String third_token_category = "";
	     
	     // Since the AnnotationsSet is not ordered we have to put it in a list with inDocumentOrder 
	     List<Annotation> TokenAnnotsList = gate.Utils.inDocumentOrder(TokenAnnots);
	     
	     for(Annotation TokenAnn : TokenAnnotsList)
	        {
	    	 // only use Annotations from type "Token"
	    	 if (TokenAnn.getType().equals("Token"))
	    	   {
	    	  	// shift third token(+category) to second token, shift second to first and fill current token into third_token 
	    		// so after the first three tokens all helper token variables are populated and the first token of the text is contained 
	    		// in first_token. 
	    		first_token = second_token;
	        	first_token_category = second_token_category;
	        	second_token = third_token;
	    		second_token_category = third_token_category;
	    		third_token = TokenAnn.getFeatures().get("string").toString();
	    		// get start and end offset of current token from TokenAnn, then use those values to get the same token in defaultAnns
	    		// and extract the category (= POS Tag)
	    		third_token_category = defaultAnnots.get(TokenAnn.getStartNode().getOffset(),TokenAnn.getEndNode().getOffset()).iterator().next().getFeatures().get("category").toString();
	    		
	    		// check if all three token helper variables are filled
	    		if (!first_token.equals(""))
	    		  {

	    			/*
	    			Patterns of tags for extracting two-word phrases from reviews
	    			(Peter D. Turney "Thumbs Up or Thumbs Down? Semantic Orientation Applied to Unsupervised Classification of Reviews"
	    			 Proceedings of ACL, July 2002, pp. 417-424, Table 1)

	    			    First Word         Second Word              Third Word (Not Extracted) 
	    			 1. JJ                 NN or NNS                anything
	    			 2. RB, RBR or RBS     JJ                       not NN nor NNS
	    			 3. JJ                 JJ                       not NN nor NNS
	    			 4. NN or NNS          JJ                       not NN nor NNS 
	    			 5. RB, RBR or RBS     VB, VBD, VBN or VBG      anything
	    			*/
	   			
	       		  // 1. JJ                 NN or NNS                anything
	    			if ( 
	    					( (first_token_category.equals("JJ")) ) && 
	    					( (second_token_category.equals("NN")) || (second_token_category.equals("NNS")) )
	    					)
	    			{
//	    				System.out.print(first_token + " (" + first_token_category + ") - " );
//	    				System.out.print(second_token + " (" + second_token_category + ") - " );
//	    				System.out.println(third_token + " (" + third_token_category + ")" );
	    				result.add(first_token+" "+second_token);
	    			}
	       		   

	       		  // 2. RB, RBR or RBS     JJ                       not NN nor NNS
	       		   if (
	       				   ( (first_token_category.equals("RB")) || (first_token_category.equals("RBR")) || (first_token_category.equals("RBS")) ) && 
	       				   ( (second_token_category.equals("JJ")) ) && 
	       				   ( (!third_token_category.equals("NN")) && (!third_token_category.equals("NNS")) )
	       				   )
	       		   {
//	   				System.out.print(first_token + " (" + first_token_category + ") - " );
//	   				System.out.print(second_token + " (" + second_token_category + ") - " );
//	   				System.out.println(third_token + " (" + third_token_category + ")" );
	   				result.add(first_token+" "+second_token);
	   			   }

	       		  // 3. JJ                 JJ                       not NN nor NNS
	       		   if (
	       				   ( (first_token_category.equals("JJ")) ) && 
	       				   ( (second_token_category.equals("JJ")) ) && 
	       				   ( (!third_token_category.equals("NN")) && (!third_token_category.equals("NNS")) )
	       				   )
	       		   {
//	   				System.out.print(first_token + " (" + first_token_category + ") - " );
//	   				System.out.print(second_token + " (" + second_token_category + ") - " );
//	   				System.out.println(third_token + " (" + third_token_category + ")" );
	   				result.add(first_token+" "+second_token);
	       		   }

	       		  // 4. NN or NNS          JJ                       not NN nor NNS 
	       		   if (
	       				   ( (first_token_category.equals("NN")) || (first_token_category.equals("NNS")) ) && 
	       				   ( (second_token_category.equals("JJ")) ) && 
	       				   ( (!third_token_category.equals("NN")) && (!third_token_category.equals("NNS")) )
	       				   )
	       		   {
//	   				System.out.print(first_token + " (" + first_token_category + ") - " );
//	   				System.out.print(second_token + " (" + second_token_category + ") - " );
//	   				System.out.println(third_token + " (" + third_token_category + ")" );
	   				result.add(first_token+" "+second_token);
	       		   }

	       		  // 5. RB, RBR or RBS     VB, VBD, VBN or VBG      anything
	       		   if (
	       				   ( (first_token_category.equals("RB")) || (first_token_category.equals("RBR")) || (first_token_category.equals("RBS")) ) && 
	       				   ( (second_token_category.equals("VB")) || (second_token_category.equals("VBD")) || (second_token_category.equals("VBN")) || (second_token_category.equals("VBG")) )
	       				   )
	       		   {
//	   				System.out.print(first_token + " (" + first_token_category + ") - " );
//	   				System.out.print(second_token + " (" + second_token_category + ") - " );
//	   				System.out.println(third_token + " (" + third_token_category + ")" );
	   				result.add(first_token+" "+second_token);
	       		   }
	 
	    		  
	    		  }	// if (!first_token.equals(""))
	    		
	//  TODO: get rid of this test code 
//	          System.out.print(TokenAnn);
//	    		System.out.print(TokenAnn.getId());
//	    		System.out.print("  ");
//	    	    System.out.print(TokenAnn.getFeatures().get("string"));
//	    	    System.out.print(" --> ");
//	   		System.out.println(defaultAnnots.get(TokenAnn.getStartNode().getOffset(),TokenAnn.getEndNode().getOffset()).iterator().next().getFeatures().get("category"));    		

	    	   }  // if (TokenAnn.getType().equals("Token"))
	    	
	        }  // for(Annotation TokenAnn : TokenAnnotsList)

//	  System.out.println();
//	  System.out.println("Tagging was successful!");

		return result;
	}
	
	
	
	
	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}	
}
