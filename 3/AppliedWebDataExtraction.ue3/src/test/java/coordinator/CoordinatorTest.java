package coordinator;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation.Coordiantor;

public class CoordinatorTest {

	@Test
	public void testReadFile(){
		
		Coordiantor co=new Coordiantor();
		try {
			co.readFileCSV("src"+File.separator+"resource"+File.separator+"hotels.csv");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(co.map.size()>0);
	}
}
