package semanticOrientation;

import java.io.IOException;

import org.junit.Test;

import at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation.SentimentOrientation;

public class SemanticOrientationTest {
	@Test
	public void testSO_negativeWords() throws IOException{
		SentimentOrientation so = new SentimentOrientation();
		
		so.addString("terrible");
		so.addString("horrendous");
		so.addString("worst");
		so.addString("useless");
		so.addString("shit");
		so.addString("catastrophic");
		so.addString("unuseable");
		
		so.calculateSentimentOrientation();
		double avgSentiment = so.getAverageSentimentOrientation();
		
		System.out.println("Average: " + avgSentiment);
	}
}
