package semanticOrientation;

import static org.junit.Assert.*;

import java.io.IOException;
import org.junit.Test;

import at.ac.tuwien.ss14.appliedWebDataExtraction.ue3.SemanticOrientation.GoogleSearch;

public class GoogleSearchTest {

	@Test
	public void testGoogleSearch() throws IOException {
		assertTrue(GoogleSearch.getSearchResults("java") > 0L);
	}

	@Test
	public void testGoogleSearchAround() throws IOException{
		assertTrue(GoogleSearch.getSearchResults("low fees", "excellent") > 0L);
	}
	
	@Test
	public void testCaching() throws IOException {
		long start = System.currentTimeMillis();
		GoogleSearch.getSearchResults("Google"); // https://www.youtube.com/watch?v=OqxLmLUT-qc 
		long duration1 = System.currentTimeMillis() - start;
		
		start = System.currentTimeMillis();
		GoogleSearch.getSearchResults("Google");
		long duration2 = System.currentTimeMillis() - start;
		
		// Duration 2 is cached and should be way faster than the initial lookup
		System.out.println(String.format("init: %d ms, cache: %d ms",  duration1, duration2));
		assertTrue(duration2 < 100);		
	}
	
}
